/* eslint-disable */

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);


const calculate = function(stats, type, isBowler){
    let bonus = 0;
    let total = 0;
    let pctc = stats.catch ? parseInt(stats.catch * 6) : 0;
    let pstm = stats.stump ? parseInt(stats.stump * 6) : 0;
    let prout = stats.run_out ? parseInt(stats.run_out * 8) : 0;
    if(type.toLowerCase() === 'test') {
        let pctc_2 = stats.catch_2 ? parseInt(stats.catch_2 * 6) : 0;
        let pstm_2 = stats.stump_2 ? parseInt(stats.stump_2 * 10) : 0;
        let prout_2 = stats.run_out_2 ? parseInt(stats.run_out_2 * 8) : 0;
        pctc = pctc + pctc_2;
        pstm = pstm + pstm_2;
        prout = prout + prout_2;
    }
    if(stats.played) {
        bonus = 5;
        let pr = 0, pw = 0;
        let tot_wckt = 0;
        if(stats.batted) {
            let p4, p6;
            pr = stats.runs ? parseInt(stats.runs) : 0;
            p4 = stats.fours ? parseInt(stats.fours) : 0;
            p6 = stats.sixs ? parseInt(stats.sixs)*2 : 0;
            bonus = bonus + p4 + p6;
        }
        switch (type.toLowerCase()) {
            case 'test':
                if(stats.batted) {
                    if(pr >= 200 ) {
                        bonus+=50;
                    } else if(pr >= 100){
                        bonus+=20;
                    } else if(pr >= 50) {
                        bonus+=10;
                    }
                    if (pr === 0 && !isBowler) {
                        bonus-=10;
                    }
                }
                if(stats.batted_2) {
                    let pr_2 = stats.runs_2 ? parseInt(stats.runs_2) : 0;
                    let p4_2 = 0, p6_2 = 0;
                    p4_2 = stats.fours_2 ? parseInt(stats.fours_2) : 0;
                    p6_2 = stats.sixs_2 ? parseInt(stats.sixs_2 * 2) : 0;
                    bonus = bonus + p4_2 + p6_2;
                    if(pr_2 >= 200 ) {
                        bonus+=50;
                    } else if(pr_2 >= 100){
                        bonus+=20;
                    } else if(pr_2 >= 50) {
                        bonus+=10;
                    }
                    if (pr_2 === 0 && !isBowler) {
                        bonus-=10;
                    }
                    pr = pr + pr_2;
                }
                if(stats.bowled) {
                    pw = stats.wickets ? parseInt(stats.wickets) * 20 : 0;
                    tot_wckt = stats.wickets ? parseInt(stats.wickets) : 0;
                }
                if(stats.bowled_2) {
                    let pw_2 = stats.wickets_2 ? parseInt(stats.wickets_2) * 20 : 0;
                    let tot_wckt_2 = stats.wickets_2 ? parseInt(stats.wickets_2) : 0;
                    tot_wckt = tot_wckt + tot_wckt_2;
                    pw = pw + pw_2;
                }
                if(tot_wckt >= 12) {
                    bonus+=60;
                } else if(tot_wckt >= 10) {
                    bonus+=50;
                } else if(tot_wckt >= 7) {
                    bonus+=30;
                } else if(tot_wckt >= 5) {
                    bonus+=10;
                }
                break;
            case 'odi':
                if(stats.batted) {
                    if(pr >= 100 ) {
                        bonus+=20;
                    } else if(pr >= 50){
                        bonus+=10;
                    }
                    if(!isBowler){
                        if (pr === 0) {
                            bonus= bonus - 10;
                        }
                        if(stats.balls && parseInt(stats.balls) >= 20) {
                            let sr = parseInt((parseInt(stats.runs) / parseInt(stats.balls))*100);
                            if(sr < 30) {
                                bonus = bonus - 6;
                            } else if(sr < 40) {
                                bonus = bonus - 4;
                            }else if(sr < 50) {
                                bonus = bonus - 2;
                            }
                        }
                    }
                }
                if(stats.bowled) {
                    pw = stats.wickets ? parseInt(stats.wickets * 20) : 0;
                    if(stats.wickets && parseInt(stats.wickets) >= 5) {
                        bonus+=20;
                    } else if(stats.wickets && parseInt(stats.wickets) === 4) {
                        bonus+=10;
                    }
                    if(stats.maidens && parseInt(stats.maidens) > 0) {
                        bonus = bonus + parseInt(stats.maidens * 4)
                    }
                    if(stats.economy && stats.overs && parseInt(stats.overs) >= 4) {
                        if(parseFloat(stats.economy) <= 3) {
                            bonus = bonus + 2;
                        } else if(parseFloat(stats.economy) <= 6){
                            bonus = bonus + 1;
                        } else if(parseFloat(stats.economy) >= 9){
                            bonus = bonus - 2;
                        } else if(parseFloat(stats.economy) >= 7){
                            bonus = bonus - 1;
                        }
                    }
                }
                break;
            case 't20':
                if(stats.batted) {
                    if(pr >= 100 ) {
                        bonus+=50;
                    } else if(pr >= 50){
                        bonus+=30;
                    } else if(pr >= 30){
                        bonus+=20;
                    }
                    if(!isBowler){
                        if (pr === 0) {
                            bonus= bonus - 10;
                        }
                        if(stats.balls && parseInt(stats.balls) >= 10) {
                            let sr = (parseInt(stats.runs) / parseInt(stats.balls))*100;
                            if(sr < 30) {
                                bonus = bonus - 8;
                            } else if(sr < 40) {
                                bonus = bonus - 6;
                            } else if(sr < 50) {
                                bonus = bonus - 4;
                            } else if(sr < 80) {
                                bonus = bonus - 2;
                            } else if(sr >= 200) {
                                bonus = bonus + 8;
                            } else if(sr >= 175) {
                                bonus = bonus + 6;
                            } else if(sr >= 150) {
                                bonus = bonus + 4;
                            } else if(sr >= 130) {
                                bonus = bonus + 2;
                            }
                        }
                    }
                }
                if(stats.bowled) {
                    pw = stats.wickets ? parseInt(stats.wickets) * 25 : 0;
                    if(stats.wickets && parseInt(stats.wickets) >= 5) {
                        bonus+=50;
                    } else if(stats.wickets && parseInt(stats.wickets) === 4) {
                        bonus+=30;
                    } else if(stats.wickets && parseInt(stats.wickets) === 3) {
                        bonus+=20;
                    } else if(stats.wickets && parseInt(stats.wickets) === 2) {
                        bonus+=10;
                    }
                    if(stats.maidens && parseInt(stats.maidens) > 0) {
                        bonus = bonus + (parseInt(stats.maidens)*10)
                    }
                    if(stats.economy && stats.overs && parseInt(stats.overs) >= 2) {
                        if(parseFloat(stats.economy) <= 4) {
                            bonus = bonus + 4;
                        } else if(parseFloat(stats.economy) <= 7){
                            bonus = bonus + 2;
                        } else if(parseFloat(stats.economy) >= 12){
                            bonus = bonus - 4;
                        } else if(parseFloat(stats.economy) >= 10){
                            bonus = bonus - 2;
                        } else if(parseFloat(stats.economy) >= 8){
                            bonus = bonus - 1;
                        }
                    }
                    if(stats.noball && parseInt(stats.noball) > 0) {
                        bonus = bonus - parseInt(stats.noball)
                    }
                }
                break;
            case 't10':
                if(stats.batted) {
                    if(pr >= 100 ) {
                        bonus+=40;
                    } else if(pr >= 50){
                        bonus+=20;
                    } else if(pr >= 30){
                        bonus+=10;
                    }
                    if(!isBowler){
                        if (pr === 0) {
                            bonus= bonus - 10;
                        }
                        if(stats.balls && parseInt(stats.balls) >= 5) {
                            let sr = parseInt((parseInt(stats.runs) / parseInt(stats.balls))*100);
                            if(sr < 30) {
                                bonus = bonus - 10;
                            } else if(sr < 40) {
                                bonus = bonus - 8;
                            } else if(sr < 50) {
                                bonus = bonus - 6;
                            } else if(sr < 80) {
                                bonus = bonus - 4;
                            } else if(sr < 100) {
                                bonus = bonus - 2;
                            }
                        }
                    }
                }
                if(stats.bowled) {
                    pw = stats.wickets ? parseInt(stats.wickets * 25) : 0;
                    if(stats.wickets && parseInt(stats.wickets) >= 4) {
                        bonus+=20;
                    } else if(stats.wickets && parseInt(stats.wickets) >= 2) {
                        bonus+=10;
                    }
                    if(stats.maidens && parseInt(stats.maidens) > 0) {
                        bonus = bonus + parseInt(stats.maidens * 10)
                    }
                    if(stats.economy && stats.overs) {
                        if(parseFloat(stats.economy) <= 6) {
                            bonus = bonus + 2;
                        } else if(parseFloat(stats.economy) <= 8){
                            bonus = bonus + 1;
                        } else if(parseFloat(stats.economy) >= 12){
                            bonus = bonus - 2;
                        } else if(parseFloat(stats.economy) >= 10){
                            bonus = bonus - 1;
                        }
                    }
                }
                break;
            default:
                break;
        }
        total = parseInt(pr + pw + pctc + pstm + prout + bonus);
    } else {
        total = parseInt(pctc + pstm + prout);
    }
    return {
        total: total,
        bonus: bonus
    }
};

const getPlayerRoundInfo = function(player, pool, round_key){
    let role = '';
    let played = 0;
    let points = 0;
    for (let j=0; j < pool.length; j++) {
        if(pool[j].key === player) {
            if(pool[j].playingRole && pool[j].playingRole.toUpperCase().includes('WICKET')) {
                role = 'wk'
            } else if(pool[j].playingRole && pool[j].playingRole.toUpperCase().includes('ALL')) {
                role = 'al'
            } else if(pool[j].playingRole && pool[j].playingRole.toUpperCase().includes('BOWLER')) {
                role = 'bow'
            } else {
                role = 'bts'
            }
            let pRounds = pool[j].rounds;
            for (let k = 0; k < pRounds.length; k++) {
                if(pRounds[k].key === round_key) {
                    played = pRounds[k].played;
                    points = pRounds[k].points;
                    break;
                }
            }
            break;
        }
    }
    return {
        role: role,
        played: played,
        points: points,
    }
};

exports.createProfile = functions.auth.user().onCreate((userRecord) => {
    return admin.database().ref('/users-role/' + userRecord.uid).set({
            email: userRecord.email,
            moderator: false
    })
});

exports.addCustomClaimsToModerators = functions.database.ref('/moderators/{uid}/').onCreate((snap) => {
    const customClaims = {
        editor: true,
        moderator: true,
    };
    return admin.auth().setCustomUserClaims(snap.key, customClaims)
});

exports.removeCustomClaimsToModerators = functions.database.ref('/moderators/{uid}/').onDelete((snap) => {
    const customClaims = {
        editor: false,
        moderator: false,
    };
    return admin.auth().setCustomUserClaims(snap.key, customClaims)
});

exports.addCustomClaimsToEditors = functions.database.ref('/editors/{uid}/').onCreate((snap) => {
    const customClaims = {
        editor: true,
        moderator: false,
    };
    return admin.auth().setCustomUserClaims(snap.key, customClaims)
});

exports.removeCustomClaimsToEditors = functions.database.ref('/editors/{uid}/').onDelete((snap) => {
    const customClaims = {
        editor: false,
        moderator: false,
    };
    return admin.auth().setCustomUserClaims(snap.key, customClaims)
});


exports.updateUsersToTournament = functions.database.ref('/tournaments/{tid}/users/{uid}/bank').onWrite((snapshot, context) => {
    let tour_key = snapshot.after.ref.parent.ref.parent.ref.parent.ref.key;
    let u_key = snapshot.after.ref.parent.ref.key;
    if(context.authType === 'USER' && context.auth.uid === u_key) {
        return admin.database().ref('/tournaments/' + tour_key + '/data/users/' + u_key).update({
            bank: value
        })
    }
});

exports.addUsersToTournament = functions.database.ref('/tournaments/{tid}/users/{uid}').onCreate((snapshot, context) => {
    let value =  snapshot.val().tm_name;
    let parentKey = snapshot.ref.parent.ref.parent.key;
    if(context.authType === 'USER') {
        return admin.database().ref('/tournaments/' + parentKey + '/data/users/' + context.auth.uid).update({
            tm_name: value,
            rank: 0,
            total_points: 0,
            bank: 100
        })
    }
});

exports.addLeaguesInPublicNode = functions.database.ref('/tournaments/{tid}/users/{uid}/leagues/{lid}').onCreate((snapshot, context) => {
    let value =  snapshot.val();
    let data = {
        league_name: value.league_name,
        max_p: value.max_p,
        code: value.code,
        owner: context.auth.uid
    };
    let parentKey = snapshot.ref.parent.ref.parent.ref.parent.ref.parent.key;
    if(context.authType === 'USER') {
        return admin.database().ref('/tournaments/' + parentKey + '/leagues/' + snapshot.key).update({
            data: data
        })
    }
});

exports.addusersInPublicLeagueNode = functions.database.ref('/tournaments/{tid}/users/{uid}/private-leagues/{lid}').onCreate((snapshot, context) => {
    let league_key = snapshot.key;
    let tournament = snapshot.ref.parent.ref.parent.ref.parent.ref.parent;
    let participants = tournament.child('leagues').child(league_key).child('participants');
    let exists = false;
    for(let p_key in participants) {
        if(participants[p_key].user_key === context.auth.uid) {
            exists = true
        }
    }
    if(!exists) {
        let newParticipant =  snapshot.val();
        newParticipant.rank = 0;
        newParticipant.total_points = 0;
        participants.push(newParticipant);
        if(context.authType === 'USER') {
            return admin.database().ref('/tournaments/' + tournament.key + '/leagues/' + league_key).update({
                participants: participants
            })
        }
    }
});

exports.removeUsersFromPublicLeagueNode = functions.database.ref('/tournaments/{tid}/leagues/{lid}/data/request_block/{id}').onCreate((snapshot, context) => {
    let blockListUserKey = snapshot.val()
    let tournament = snapshot.ref.parent.ref.parent.ref.parent.ref.parent.ref.parent.key;
    let league_key = snapshot.ref.parent.ref.parent.ref.parent.key;
    if(context.authType === 'USER') {
        admin.database().ref('/tournaments/' + tournament + '/leagues/' + league_key + '/participants/' + blockListUserKey.pid)
            .set(null).then(() => {
                return admin.database().ref('/tournaments/' + tournament + '/users/' + blockListUserKey.user_key + '/private-leagues/' + league_key).set(null)
            }).catch((err)=> console.log(err))
    }
});

exports.addUsersToSingleMatch = functions.database.ref('/single-matches/{tid}/users/{uid}').onCreate((snapshot, context) => {
    let parentKey = snapshot.ref.parent.ref.parent.key;
    if(context.authType === 'USER') {
        return admin.database().ref('/single-matches/' + parentKey + '/data/users/' + context.auth.uid).update({
            rank: 0,
            total_points: 0
        })
    }
});

exports.updateScoreToSingleMatch = functions.database.ref('/scorer/single-match/{mid}/match').onUpdate((snapshot) => {
    let match_info = snapshot.after.val();
    let match_key = snapshot.after.ref.parent.ref.key;
    let type = match_info.match_type.toLowerCase();
    admin.database().ref('single-matches/' + match_key + '/data/pool/').once('value', function(playerSnaps){
        let all_players = playerSnaps.val()
        let tm_1 = match_info.tm_1_xi
        for(let key in tm_1) {
            let stats = {
                played: 1
            };
            stats.run_out = tm_1[key].run_out ? parseInt(tm_1[key].run_out) : 0;
            stats.catch = tm_1[key].catch ? parseInt(tm_1[key].catch) : 0;
            stats.stump = tm_1[key].stump ? parseInt(tm_1[key].stump) : 0;
            if(tm_1[key].dnbt) {
                stats.batted = 0;
                stats.runs = 0;
                stats.balls = 0;
                stats.fours = 0;
                stats.sixs = 0;
                stats.srate = 0;
            }
            else {
                stats.batted = 1;
                stats.runs = tm_1[key].runs ? parseInt(tm_1[key].runs) : 0;
                stats.balls = tm_1[key].balls ? parseInt(tm_1[key].balls) : 0;
                stats.fours = tm_1[key].fours ? parseInt(tm_1[key].fours) : 0;
                stats.sixs = tm_1[key].sixs ? parseInt(tm_1[key].sixs) : 0;
                stats.srate = tm_1[key].srate ? parseFloat(tm_1[key].srate) : 0;
            }
            if(tm_1[key].dnbl){
                stats.bowled = 0;
                stats.overs = 0;
                stats.maidens = 0;
                stats.wickets = 0;
                stats.g_run = 0;
                stats.economy = 0;
            }
            else {
                stats.bowled = 1;
                stats.overs = tm_1[key].overs ? parseInt(tm_1[key].overs) : 0;
                stats.maidens = tm_1[key].maidens ? parseInt(tm_1[key].maidens) : 0;
                stats.wickets = tm_1[key].wickets ? parseInt(tm_1[key].wickets) : 0;
                stats.g_run = tm_1[key].given_runs ? parseInt(tm_1[key].given_runs) : 0;
                stats.economy = tm_1[key].economy ? parseFloat(tm_1[key].economy) : 0;
            }
            if(type === 'test') {
                stats.run_out_2 = tm_1[key].run_out_2 ? parseInt(tm_1[key].run_out_2) : 0;
                stats.catch_2 = tm_1[key].catch_2 ? parseInt(tm_1[key].catch_2) : 0;
                stats.stump_2 = tm_1[key].stump_2 ? parseInt(tm_1[key].stump_2) : 0;
                if(tm_1[key].dnbt_2){
                    stats.batted_2 = 0;
                    stats.runs_2 = 0;
                    stats.balls_2 = 0;
                    stats.fours_2 = 0;
                    stats.sixs_2 = 0;
                    stats.srate_2 = 0;
                } else {
                    stats.batted_2 = 1;
                    stats.runs_2 = tm_1[key].runs_2 ? parseInt(tm_1[key].runs_2) : 0;
                    stats.balls_2 = tm_1[key].balls_2 ? parseInt(tm_1[key].balls_2) : 0;
                    stats.fours_2 = tm_1[key].fours_2 ? parseInt(tm_1[key].fours_2) : 0;
                    stats.sixs_2 = tm_1[key].sixs_2 ? parseInt(tm_1[key].sixs_2) : 0;
                    stats.srate_2 = tm_1[key].srate_2 ? parseFloat(tm_1[key].srate_2) : 0;
                }
                if(tm_1[key].dnbl_2){
                    stats.bowled_2 = 0;
                    stats.overs_2 = 0;
                    stats.maidens_2 = 0;
                    stats.wickets_2 = 0;
                    stats.g_run_2 = 0;
                    stats.economy_2 = 0;
                } else {
                    stats.bowled_2 = 1;
                    stats.overs_2 = tm_1[key].overs_2 ? parseInt(tm_1[key].overs_2) : 0;
                    stats.maidens_2 = tm_1[key].maidens_2 ? parseInt(tm_1[key].maidens_2) : 0;
                    stats.wickets_2 = tm_1[key].wickets_2 ? parseInt(tm_1[key].wickets_2) : 0;
                    stats.g_run_2 = tm_1[key].given_runs_2 ? parseInt(tm_1[key].given_runs_2) : 0;
                    stats.economy_2 = tm_1[key].economy_2 ? parseFloat(tm_1[key].economy_2) : 0;
                }
            }
            for (let p_key in all_players) {
                if (all_players[p_key].key === tm_1[key].key) {
                    let role = all_players[p_key].playingRole.toLowerCase().includes('bowler');
                    let points = calculate(stats, type, role)
                    all_players[p_key].stats = stats;
                    all_players[p_key].bonus = points.bonus;
                    all_players[p_key].total_points = points.total;
                    break;
                }
            }
        }
        let tm_2 = match_info.tm_2_xi
        for(let key in tm_2) {
            let stats = {
                played: 1
            };
            stats.run_out = tm_2[key].run_out ? parseInt(tm_2[key].run_out) : 0;
            stats.catch = tm_2[key].catch ? parseInt(tm_2[key].catch) : 0;
            stats.stump = tm_2[key].stump ? parseInt(tm_2[key].stump) : 0;
            if(tm_2[key].dnbt) {
                stats.batted = 0;
                stats.runs = 0;
                stats.balls = 0;
                stats.fours = 0;
                stats.sixs = 0;
                stats.srate = 0;
            }
            else {
                stats.batted = 1;
                stats.runs = tm_2[key].runs ? parseInt(tm_2[key].runs) : 0;
                stats.balls = tm_2[key].balls ? parseInt(tm_2[key].balls) : 0;
                stats.fours = tm_2[key].fours ? parseInt(tm_2[key].fours) : 0;
                stats.sixs = tm_2[key].sixs ? parseInt(tm_2[key].sixs) : 0;
                stats.srate = tm_2[key].srate ? parseFloat(tm_2[key].srate) : 0;
            }
            if(tm_2[key].dnbl){
                stats.bowled = 0;
                stats.overs = 0;
                stats.maidens = 0;
                stats.wickets = 0;
                stats.g_run = 0;
                stats.economy = 0;
            }
            else {
                stats.bowled = 1;
                stats.overs = tm_2[key].overs ? parseInt(tm_2[key].overs) : 0;
                stats.maidens = tm_2[key].maidens ? parseInt(tm_2[key].maidens) : 0;
                stats.wickets = tm_2[key].wickets ? parseInt(tm_2[key].wickets) : 0;
                stats.g_run = tm_2[key].given_runs ? parseInt(tm_2[key].given_runs) : 0;
                stats.economy = tm_2[key].economy ? parseFloat(tm_2[key].economy) : 0;
            }
            if(type === 'test') {
                stats.run_out_2 = tm_2[key].run_out_2 ? parseInt(tm_2[key].run_out_2) : 0;
                stats.catch_2 = tm_2[key].catch_2 ? parseInt(tm_2[key].catch_2) : 0;
                stats.stump_2 = tm_2[key].stump_2 ? parseInt(tm_2[key].stump_2) : 0;
                if(tm_2[key].dnbt_2){
                    stats.batted_2 = 0;
                    stats.runs_2 = 0;
                    stats.balls_2 = 0;
                    stats.fours_2 = 0;
                    stats.sixs_2 = 0;
                    stats.srate_2 = 0;
                } else {
                    stats.batted_2 = 1;
                    stats.runs_2 = tm_2[key].runs_2 ? parseInt(tm_2[key].runs_2) : 0;
                    stats.balls_2 = tm_2[key].balls_2 ? parseInt(tm_2[key].balls_2) : 0;
                    stats.fours_2 = tm_2[key].fours_2 ? parseInt(tm_2[key].fours_2) : 0;
                    stats.sixs_2 = tm_2[key].sixs_2 ? parseInt(tm_2[key].sixs_2) : 0;
                    stats.srate_2 = tm_2[key].srate_2 ? parseFloat(tm_2[key].srate_2) : 0;
                }
                if(tm_2[key].dnbl_2){
                    stats.bowled_2 = 0;
                    stats.overs_2 = 0;
                    stats.maidens_2 = 0;
                    stats.wickets_2 = 0;
                    stats.g_run_2 = 0;
                    stats.economy_2 = 0;
                } else {
                    stats.bowled_2 = 1;
                    stats.overs_2 = tm_2[key].overs_2 ? parseInt(tm_2[key].overs_2) : 0;
                    stats.maidens_2 = tm_2[key].maidens_2 ? parseInt(tm_2[key].maidens_2) : 0;
                    stats.wickets_2 = tm_2[key].wickets_2 ? parseInt(tm_2[key].wickets_2) : 0;
                    stats.g_run_2 = tm_2[key].given_runs_2 ? parseInt(tm_2[key].given_runs_2) : 0;
                    stats.economy_2 = tm_2[key].economy_2 ? parseFloat(tm_2[key].economy_2) : 0;
                }
            }
            for (let p_key in all_players) {
                if (all_players[p_key].key === tm_2[key].key) {
                    let role = all_players[p_key].playingRole.toLowerCase().includes('bowler');
                    let points = calculate(stats, type, role)
                    all_players[p_key].stats = stats;
                    all_players[p_key].bonus = points.bonus;
                    all_players[p_key].total_points = points.total;
                    break;
                }
            }
        }
        admin.database().ref('/single-matches/' + match_key + '/data/').update({
            pool: all_players
        }).then(()=> {
            return null
        }).catch((err)=> console.log(err))
    })
});

exports.updateScoreToTournament = functions.database.ref('/scorer/tournaments/{tid}/rounds/{rid}/match/{mid}').onUpdate((snapshot) => {
    let match_info = snapshot.after.val();
    let tour_key = snapshot.after.ref.parent.ref.parent.ref.parent.ref.parent.ref.key;
    let round_key = snapshot.after.ref.parent.ref.parent.ref.key;
    let match_key = snapshot.after.ref.key;
    let type = match_info.match_type.toLowerCase();
    admin.database().ref('tournaments/' + tour_key + '/pool/').once('value', function(playerSnaps){
        let all_players = playerSnaps.val()
        let tm_1 = match_info.tm_1_xi
        for(let key in tm_1) {
            let stats = {
                played: 1
            };
            stats.run_out = tm_1[key].run_out ? parseInt(tm_1[key].run_out) : 0;
            stats.catch = tm_1[key].catch ? parseInt(tm_1[key].catch) : 0;
            stats.stump = tm_1[key].stump ? parseInt(tm_1[key].stump) : 0;
            if(tm_1[key].dnbt) {
                stats.batted = 0;
                stats.runs = 0;
                stats.balls = 0;
                stats.fours = 0;
                stats.sixs = 0;
                stats.srate = 0;
            }
            else {
                stats.batted = 1;
                stats.runs = tm_1[key].runs ? parseInt(tm_1[key].runs) : 0;
                stats.balls = tm_1[key].balls ? parseInt(tm_1[key].balls) : 0;
                stats.fours = tm_1[key].fours ? parseInt(tm_1[key].fours) : 0;
                stats.sixs = tm_1[key].sixs ? parseInt(tm_1[key].sixs) : 0;
                stats.srate = tm_1[key].srate ? parseFloat(tm_1[key].srate) : 0;
            }
            if(tm_1[key].dnbl){
                stats.bowled = 0;
                stats.overs = 0;
                stats.maidens = 0;
                stats.wickets = 0;
                stats.g_run = 0;
                stats.economy = 0;
            }
            else {
                stats.bowled = 1;
                stats.overs = tm_1[key].overs ? parseInt(tm_1[key].overs) : 0;
                stats.maidens = tm_1[key].maidens ? parseInt(tm_1[key].maidens) : 0;
                stats.wickets = tm_1[key].wickets ? parseInt(tm_1[key].wickets) : 0;
                stats.g_run = tm_1[key].given_runs ? parseInt(tm_1[key].given_runs) : 0;
                stats.economy = tm_1[key].economy ? parseFloat(tm_1[key].economy) : 0;
            }
            if(type === 'test') {
                stats.run_out_2 = tm_1[key].run_out_2 ? parseInt(tm_1[key].run_out_2) : 0;
                stats.catch_2 = tm_1[key].catch_2 ? parseInt(tm_1[key].catch_2) : 0;
                stats.stump_2 = tm_1[key].stump_2 ? parseInt(tm_1[key].stump_2) : 0;
                if(tm_1[key].dnbt_2){
                    stats.batted_2 = 0;
                    stats.runs_2 = 0;
                    stats.balls_2 = 0;
                    stats.fours_2 = 0;
                    stats.sixs_2 = 0;
                    stats.srate_2 = 0;
                } else {
                    stats.batted_2 = 1;
                    stats.runs_2 = tm_1[key].runs_2 ? parseInt(tm_1[key].runs_2) : 0;
                    stats.balls_2 = tm_1[key].balls_2 ? parseInt(tm_1[key].balls_2) : 0;
                    stats.fours_2 = tm_1[key].fours_2 ? parseInt(tm_1[key].fours_2) : 0;
                    stats.sixs_2 = tm_1[key].sixs_2 ? parseInt(tm_1[key].sixs_2) : 0;
                    stats.srate_2 = tm_1[key].srate_2 ? parseFloat(tm_1[key].srate_2) : 0;
                }
                if(tm_1[key].dnbl_2){
                    stats.bowled_2 = 0;
                    stats.overs_2 = 0;
                    stats.maidens_2 = 0;
                    stats.wickets_2 = 0;
                    stats.g_run_2 = 0;
                    stats.economy_2 = 0;
                } else {
                    stats.bowled_2 = 1;
                    stats.overs_2 = tm_1[key].overs_2 ? parseInt(tm_1[key].overs_2) : 0;
                    stats.maidens_2 = tm_1[key].maidens_2 ? parseInt(tm_1[key].maidens_2) : 0;
                    stats.wickets_2 = tm_1[key].wickets_2 ? parseInt(tm_1[key].wickets_2) : 0;
                    stats.g_run_2 = tm_1[key].given_runs_2 ? parseInt(tm_1[key].given_runs_2) : 0;
                    stats.economy_2 = tm_1[key].economy_2 ? parseFloat(tm_1[key].economy_2) : 0;
                }
            }
            for (let p_key in all_players) {
                if (all_players[p_key].key === tm_1[key].key) {
                    let role = all_players[p_key].playingRole.toLowerCase().includes('bowler');
                    let points = calculate(stats, type, role);
                    for(let r = 0; r < all_players[p_key].rounds.length; r++){
                        if(all_players[p_key].rounds[r].key === round_key) {
                            all_players[p_key].rounds[r].played = 1;
                            let tot = 0;
                            if(all_players[p_key].rounds[r].matches) {
                                let found  = false;
                                for(let m in all_players[p_key].rounds[r].matches) {
                                    if(m === match_key) {
                                        found = true;
                                        all_players[p_key].rounds[r].matches[m].stats = stats;
                                        all_players[p_key].rounds[r].matches[m].bonus = points.bonus;
                                        all_players[p_key].rounds[r].matches[m].points = points.total;
                                    }
                                    tot = tot + all_players[p_key].rounds[r].matches[m].points
                                }
                                if(!found){
                                    let data = {
                                        stats: stats,
                                        bonus: points.bonus,
                                        points: points.total
                                    };
                                    all_players[p_key].rounds[r].matches[match_key] = data
                                    tot = tot + points.total
                                }
                            } else {
                                let data = {
                                    stats: stats,
                                    bonus: points.bonus,
                                    points: points.total
                                };
                                all_players[p_key].rounds[r]['matches'] = {}
                                all_players[p_key].rounds[r]['matches'][match_key] = data
                                tot = tot + points.total
                            }
                            all_players[p_key].rounds[r].points = tot;
                            break;
                        }
                    }
                    break;
                }
            }
        }
        let tm_2 = match_info.tm_2_xi
        for(let key in tm_2) {
            let stats = {
                played: 1
            };
            stats.run_out = tm_2[key].run_out ? parseInt(tm_2[key].run_out) : 0;
            stats.catch = tm_2[key].catch ? parseInt(tm_2[key].catch) : 0;
            stats.stump = tm_2[key].stump ? parseInt(tm_2[key].stump) : 0;
            if(tm_2[key].dnbt) {
                stats.batted = 0;
                stats.runs = 0;
                stats.balls = 0;
                stats.fours = 0;
                stats.sixs = 0;
                stats.srate = 0;
            }
            else {
                stats.batted = 1;
                stats.runs = tm_2[key].runs ? parseInt(tm_2[key].runs) : 0;
                stats.balls = tm_2[key].balls ? parseInt(tm_2[key].balls) : 0;
                stats.fours = tm_2[key].fours ? parseInt(tm_2[key].fours) : 0;
                stats.sixs = tm_2[key].sixs ? parseInt(tm_2[key].sixs) : 0;
                stats.srate = tm_2[key].srate ? parseFloat(tm_2[key].srate) : 0;
            }
            if(tm_2[key].dnbl){
                stats.bowled = 0;
                stats.overs = 0;
                stats.maidens = 0;
                stats.wickets = 0;
                stats.g_run = 0;
                stats.economy = 0;
            }
            else {
                stats.bowled = 1;
                stats.overs = tm_2[key].overs ? parseInt(tm_2[key].overs) : 0;
                stats.maidens = tm_2[key].maidens ? parseInt(tm_2[key].maidens) : 0;
                stats.wickets = tm_2[key].wickets ? parseInt(tm_2[key].wickets) : 0;
                stats.g_run = tm_2[key].given_runs ? parseInt(tm_2[key].given_runs) : 0;
                stats.economy = tm_2[key].economy ? parseFloat(tm_2[key].economy) : 0;
            }
            if(type === 'test') {
                stats.run_out_2 = tm_2[key].run_out_2 ? parseInt(tm_2[key].run_out_2) : 0;
                stats.catch_2 = tm_2[key].catch_2 ? parseInt(tm_2[key].catch_2) : 0;
                stats.stump_2 = tm_2[key].stump_2 ? parseInt(tm_2[key].stump_2) : 0;
                if(tm_2[key].dnbt_2){
                    stats.batted_2 = 0;
                    stats.runs_2 = 0;
                    stats.balls_2 = 0;
                    stats.fours_2 = 0;
                    stats.sixs_2 = 0;
                    stats.srate_2 = 0;
                } else {
                    stats.batted_2 = 1;
                    stats.runs_2 = tm_2[key].runs_2 ? parseInt(tm_2[key].runs_2) : 0;
                    stats.balls_2 = tm_2[key].balls_2 ? parseInt(tm_2[key].balls_2) : 0;
                    stats.fours_2 = tm_2[key].fours_2 ? parseInt(tm_2[key].fours_2) : 0;
                    stats.sixs_2 = tm_2[key].sixs_2 ? parseInt(tm_2[key].sixs_2) : 0;
                    stats.srate_2 = tm_2[key].srate_2 ? parseFloat(tm_2[key].srate_2) : 0;
                }
                if(tm_2[key].dnbl_2){
                    stats.bowled_2 = 0;
                    stats.overs_2 = 0;
                    stats.maidens_2 = 0;
                    stats.wickets_2 = 0;
                    stats.g_run_2 = 0;
                    stats.economy_2 = 0;
                } else {
                    stats.bowled_2 = 1;
                    stats.overs_2 = tm_2[key].overs_2 ? parseInt(tm_2[key].overs_2) : 0;
                    stats.maidens_2 = tm_2[key].maidens_2 ? parseInt(tm_2[key].maidens_2) : 0;
                    stats.wickets_2 = tm_2[key].wickets_2 ? parseInt(tm_2[key].wickets_2) : 0;
                    stats.g_run_2 = tm_2[key].given_runs_2 ? parseInt(tm_2[key].given_runs_2) : 0;
                    stats.economy_2 = tm_2[key].economy_2 ? parseFloat(tm_2[key].economy_2) : 0;
                }
            }
            for (let p_key in all_players) {
                if (all_players[p_key].key === tm_2[key].key) {
                    let role = all_players[p_key].playingRole.toLowerCase().includes('bowler');
                    let points = calculate(stats, type, role);
                    for(let r = 0; r < all_players[p_key].rounds.length; r++){
                        if(all_players[p_key].rounds[r].key === round_key) {
                            all_players[p_key].rounds[r].played = 1;
                            let tot = 0;
                            if(all_players[p_key].rounds[r].matches) {
                                let found  = false;
                                for(let m in all_players[p_key].rounds[r].matches) {
                                    if(m === match_key) {
                                        found = true;
                                        all_players[p_key].rounds[r].matches[m].stats = stats;
                                        all_players[p_key].rounds[r].matches[m].bonus = points.bonus;
                                        all_players[p_key].rounds[r].matches[m].points = points.total;
                                    }
                                    tot = tot + all_players[p_key].rounds[r].matches[m].points
                                }
                                if(!found){
                                    let data = {
                                        stats: stats,
                                        bonus: points.bonus,
                                        points: points.total
                                    };
                                    all_players[p_key].rounds[r].matches[match_key] = data
                                    tot = tot + points.total
                                }
                            } else {
                                let data = {
                                    stats: stats,
                                    bonus: points.bonus,
                                    points: points.total
                                };
                                all_players[p_key].rounds[r]['matches'] = {}
                                all_players[p_key].rounds[r]['matches'][match_key] = data
                                tot = tot + points.total
                            }
                            all_players[p_key].rounds[r].points = tot;
                            break;
                        }
                    }
                    break;
                }
            }
        }
        admin.database().ref('tournaments/' + tour_key).update({
            pool: all_players,
            score_updated_on: new Date().toISOString()
        }).then(()=> {
            return null
        }).catch((err)=> console.log(err))
    })
});

exports.updateTotalPointsUserTournament = functions.database.ref('tournaments/{tid}/data/updated_info').onUpdate((snapshot) => {
    let value = snapshot.after.val();
    let round_key = value.round_key;
    let tour_key = snapshot.after.ref.parent.ref.parent.ref.key;
    admin.database().ref('tournaments/' + tour_key).once('value', function(snaps){
        let obj = snaps.val().users;
        let pool = snaps.val().pool;
        let t_data = snaps.val().data;
        let t_users = snaps.val().data.users;
        let max = 0;
        let min = 0;
        let count = 0;
        let all_total = 0;
        let min_id = '';
        let max_id = '';
        for (let key in obj) {
            let totalPoints = 0;
            if(obj[key].tm_crt){
                count++;
                let rounds = obj[key].rounds;
                let i, uRound, uPlayers;
                for(i = 0; i < rounds.length; i++) {
                    if(rounds[i].key === round_key) {
                        uRound = rounds[i];
                        break;
                    }
                }
                if(uRound) {
                    let deduct = uRound.deduct_points ? parseInt(uRound.deduct_points) : 0
                    let players = uRound.in_eleven_wk;
                    players = players.concat(uRound.in_eleven_bts)
                    players = players.concat(uRound.in_eleven_al)
                    players = players.concat(uRound.in_eleven_bow)
                    uPlayers = players;
                    for(let i=0; i < uPlayers.length; i++) {
                        let info = getPlayerRoundInfo(uPlayers[i], pool, round_key)
                        if(info.played) {
                            if(uPlayers[i] === uRound.cap) {
                                totalPoints = totalPoints + parseFloat(info.points * 2);
                            } else if(uPlayers[i] === uRound.vc){
                                totalPoints = totalPoints + parseFloat(info.points * 1.5);
                            } else {
                                totalPoints = totalPoints + info.points;
                            }
                        }
                    }
                    totalPoints = totalPoints - deduct;
                }
                all_total = parseFloat(all_total + totalPoints);
                if(totalPoints >= max) {
                    max = totalPoints;
                    max_id = key;

                }
                if(count === 1) {
                    min = totalPoints
                    min_id = key;
                } else if(totalPoints < min){
                    min = totalPoints;
                    min_id = key;
                }
            }
            if(t_users[key]) {
                if(t_users[key].rounds) {
                    t_users[key].rounds[round_key] = totalPoints
                } else {
                    t_users[key].rounds = {};
                    t_users[key].rounds[round_key] = totalPoints
                }
            }
        }
        if(count > 0) {
            let avg = parseFloat(parseFloat(all_total/count).toFixed())
            for(let i = 0; i < t_data.rounds.length; i++) {
                if(t_data.rounds[i].key === round_key) {
                    t_data.rounds[i].points_added = true;
                    t_data.rounds[i].min_points = min;
                    t_data.rounds[i].max_points = max;
                    t_data.rounds[i].average = avg;
                    t_data.rounds[i].min_id = min_id;
                    t_data.rounds[i].max_id = max_id;
                }
            }
            let objects = t_users;
            for (let u_key in objects) {
                let total = 0;
                let array = objects[u_key];
                for (let r_key in array.rounds) {
                    total = total + array.rounds[r_key]
                }
                objects[u_key].total_points = total
            }
            let sortable = [];
            for (let key in objects) {
                sortable.push([key, objects[key].total_points]);
            }
            sortable.sort((a, b) => (parseFloat(a[1]) < parseFloat(b[1]) ? 1 : -1));
            let rank = 1;
            let prev = sortable[0][1];
            for (let i = 0; i < sortable.length; i++) {
                for (let key in objects) {
                    if(key === sortable[i][0]) {
                        if(parseFloat(sortable[i][1]) < parseFloat(prev)) {
                            rank++
                        }
                        objects[key].prev_rank = objects[key].rank ? objects[key].rank : 0;
                        objects[key].rank = rank;
                        prev = sortable[i][1];
                        break;
                    }
                }
            }
            return admin.database().ref('tournaments/' + tour_key + '/data').update({
                users: t_users,
                rounds: t_data.rounds,
                user_score_updated_on: new Date().toISOString()
            });
        }
    })
});

exports.updateCurrentTimeStamp = functions.database.ref('time_now').onUpdate(() => {
    setTimeout(function () {
        return admin.database().ref('time_now').update({
            timestamp: new Date().getTime()
        })
    }, 60000)
});

exports.addUsersToSeries = functions.database.ref('/series/{tid}/users/{uid}').onCreate((snapshot, context) => {
    let value =  snapshot.val().tm_name;
    let parentKey = snapshot.ref.parent.ref.parent.key;
    if(context.authType === 'USER') {
        return admin.database().ref('/series/' + parentKey + '/data/users/' + context.auth.uid).update({
            tm_name: value,
            rank: 0,
            total_points: 0,
            bank: 100
        })
    }
});

exports.updateScoreToSeries = functions.database.ref('/scorer/series/{tid}/rounds/{rid}/match/{mid}').onUpdate((snapshot) => {
    let match_info = snapshot.after.val();
    let tour_key = snapshot.after.ref.parent.ref.parent.ref.parent.ref.parent.ref.key;
    let round_key = snapshot.after.ref.parent.ref.parent.ref.key;
    let match_key = snapshot.after.ref.key;
    let type = match_info.match_type.toLowerCase();
    admin.database().ref('series/' + tour_key + '/pool/').once('value', function(playerSnaps){
        let all_players = playerSnaps.val()
        let tm_1 = match_info.tm_1_xi
        for(let key in tm_1) {
            let stats = {
                played: 1
            };
            stats.run_out = tm_1[key].run_out ? parseInt(tm_1[key].run_out) : 0;
            stats.catch = tm_1[key].catch ? parseInt(tm_1[key].catch) : 0;
            stats.stump = tm_1[key].stump ? parseInt(tm_1[key].stump) : 0;
            if(tm_1[key].dnbt) {
                stats.batted = 0;
                stats.runs = 0;
                stats.balls = 0;
                stats.fours = 0;
                stats.sixs = 0;
                stats.srate = 0;
            }
            else {
                stats.batted = 1;
                stats.runs = tm_1[key].runs ? parseInt(tm_1[key].runs) : 0;
                stats.balls = tm_1[key].balls ? parseInt(tm_1[key].balls) : 0;
                stats.fours = tm_1[key].fours ? parseInt(tm_1[key].fours) : 0;
                stats.sixs = tm_1[key].sixs ? parseInt(tm_1[key].sixs) : 0;
                stats.srate = tm_1[key].srate ? parseFloat(tm_1[key].srate) : 0;
            }
            if(tm_1[key].dnbl){
                stats.bowled = 0;
                stats.overs = 0;
                stats.maidens = 0;
                stats.wickets = 0;
                stats.g_run = 0;
                stats.economy = 0;
                stats.wide = 0;
                stats.noball = 0;
            }
            else {
                stats.bowled = 1;
                stats.overs = tm_1[key].overs ? parseInt(tm_1[key].overs) : 0;
                stats.maidens = tm_1[key].maidens ? parseInt(tm_1[key].maidens) : 0;
                stats.wickets = tm_1[key].wickets ? parseInt(tm_1[key].wickets) : 0;
                stats.g_run = tm_1[key].given_runs ? parseInt(tm_1[key].given_runs) : 0;
                stats.economy = tm_1[key].economy ? parseFloat(tm_1[key].economy) : 0;
                stats.wide = tm_1[key].wide ? parseInt(tm_1[key].wide) : 0;
                stats.noball = tm_1[key].noball ? parseInt(tm_1[key].noball) : 0;
            }
            if(type === 'test') {
                stats.run_out_2 = tm_1[key].run_out_2 ? parseInt(tm_1[key].run_out_2) : 0;
                stats.catch_2 = tm_1[key].catch_2 ? parseInt(tm_1[key].catch_2) : 0;
                stats.stump_2 = tm_1[key].stump_2 ? parseInt(tm_1[key].stump_2) : 0;
                if(tm_1[key].dnbt_2){
                    stats.batted_2 = 0;
                    stats.runs_2 = 0;
                    stats.balls_2 = 0;
                    stats.fours_2 = 0;
                    stats.sixs_2 = 0;
                    stats.srate_2 = 0;
                } else {
                    stats.batted_2 = 1;
                    stats.runs_2 = tm_1[key].runs_2 ? parseInt(tm_1[key].runs_2) : 0;
                    stats.balls_2 = tm_1[key].balls_2 ? parseInt(tm_1[key].balls_2) : 0;
                    stats.fours_2 = tm_1[key].fours_2 ? parseInt(tm_1[key].fours_2) : 0;
                    stats.sixs_2 = tm_1[key].sixs_2 ? parseInt(tm_1[key].sixs_2) : 0;
                    stats.srate_2 = tm_1[key].srate_2 ? parseFloat(tm_1[key].srate_2) : 0;
                }
                if(tm_1[key].dnbl_2){
                    stats.bowled_2 = 0;
                    stats.overs_2 = 0;
                    stats.maidens_2 = 0;
                    stats.wickets_2 = 0;
                    stats.g_run_2 = 0;
                    stats.economy_2 = 0;
                } else {
                    stats.bowled_2 = 1;
                    stats.overs_2 = tm_1[key].overs_2 ? parseInt(tm_1[key].overs_2) : 0;
                    stats.maidens_2 = tm_1[key].maidens_2 ? parseInt(tm_1[key].maidens_2) : 0;
                    stats.wickets_2 = tm_1[key].wickets_2 ? parseInt(tm_1[key].wickets_2) : 0;
                    stats.g_run_2 = tm_1[key].given_runs_2 ? parseInt(tm_1[key].given_runs_2) : 0;
                    stats.economy_2 = tm_1[key].economy_2 ? parseFloat(tm_1[key].economy_2) : 0;
                }
            }
            for (let p_key in all_players) {
                if (all_players[p_key].key === tm_1[key].key) {
                    let role = all_players[p_key].playingRole.toLowerCase().includes('bowler');
                    let points = calculate(stats, type, role);
                    for(let r = 0; r < all_players[p_key].rounds.length; r++){
                        if(all_players[p_key].rounds[r].key === round_key) {
                            all_players[p_key].rounds[r].played = 1;
                            let tot = 0;
                            if(all_players[p_key].rounds[r].matches) {
                                let found  = false;
                                for(let m in all_players[p_key].rounds[r].matches) {
                                    if(m === match_key) {
                                        found = true;
                                        all_players[p_key].rounds[r].matches[m].stats = stats;
                                        all_players[p_key].rounds[r].matches[m].bonus = points.bonus;
                                        all_players[p_key].rounds[r].matches[m].points = points.total;
                                    }
                                    tot = tot + all_players[p_key].rounds[r].matches[m].points
                                }
                                if(!found){
                                    let data = {
                                        stats: stats,
                                        bonus: points.bonus,
                                        points: points.total
                                    };
                                    all_players[p_key].rounds[r].matches[match_key] = data
                                    tot = tot + points.total
                                }
                            } else {
                                let data = {
                                    stats: stats,
                                    bonus: points.bonus,
                                    points: points.total
                                };
                                all_players[p_key].rounds[r]['matches'] = {}
                                all_players[p_key].rounds[r]['matches'][match_key] = data
                                tot = tot + points.total
                            }
                            all_players[p_key].rounds[r].points = tot;
                            break;
                        }
                    }
                    break;
                }
            }
        }
        let tm_2 = match_info.tm_2_xi
        for(let key in tm_2) {
            let stats = {
                played: 1
            };
            stats.run_out = tm_2[key].run_out ? parseInt(tm_2[key].run_out) : 0;
            stats.catch = tm_2[key].catch ? parseInt(tm_2[key].catch) : 0;
            stats.stump = tm_2[key].stump ? parseInt(tm_2[key].stump) : 0;
            if(tm_2[key].dnbt) {
                stats.batted = 0;
                stats.runs = 0;
                stats.balls = 0;
                stats.fours = 0;
                stats.sixs = 0;
                stats.srate = 0;
            }
            else {
                stats.batted = 1;
                stats.runs = tm_2[key].runs ? parseInt(tm_2[key].runs) : 0;
                stats.balls = tm_2[key].balls ? parseInt(tm_2[key].balls) : 0;
                stats.fours = tm_2[key].fours ? parseInt(tm_2[key].fours) : 0;
                stats.sixs = tm_2[key].sixs ? parseInt(tm_2[key].sixs) : 0;
                stats.srate = tm_2[key].srate ? parseFloat(tm_2[key].srate) : 0;
            }
            if(tm_2[key].dnbl){
                stats.bowled = 0;
                stats.overs = 0;
                stats.maidens = 0;
                stats.wickets = 0;
                stats.g_run = 0;
                stats.economy = 0;
            }
            else {
                stats.bowled = 1;
                stats.overs = tm_2[key].overs ? parseInt(tm_2[key].overs) : 0;
                stats.maidens = tm_2[key].maidens ? parseInt(tm_2[key].maidens) : 0;
                stats.wickets = tm_2[key].wickets ? parseInt(tm_2[key].wickets) : 0;
                stats.g_run = tm_2[key].given_runs ? parseInt(tm_2[key].given_runs) : 0;
                stats.economy = tm_2[key].economy ? parseFloat(tm_2[key].economy) : 0;
            }
            if(type === 'test') {
                stats.run_out_2 = tm_2[key].run_out_2 ? parseInt(tm_2[key].run_out_2) : 0;
                stats.catch_2 = tm_2[key].catch_2 ? parseInt(tm_2[key].catch_2) : 0;
                stats.stump_2 = tm_2[key].stump_2 ? parseInt(tm_2[key].stump_2) : 0;
                if(tm_2[key].dnbt_2){
                    stats.batted_2 = 0;
                    stats.runs_2 = 0;
                    stats.balls_2 = 0;
                    stats.fours_2 = 0;
                    stats.sixs_2 = 0;
                    stats.srate_2 = 0;
                } else {
                    stats.batted_2 = 1;
                    stats.runs_2 = tm_2[key].runs_2 ? parseInt(tm_2[key].runs_2) : 0;
                    stats.balls_2 = tm_2[key].balls_2 ? parseInt(tm_2[key].balls_2) : 0;
                    stats.fours_2 = tm_2[key].fours_2 ? parseInt(tm_2[key].fours_2) : 0;
                    stats.sixs_2 = tm_2[key].sixs_2 ? parseInt(tm_2[key].sixs_2) : 0;
                    stats.srate_2 = tm_2[key].srate_2 ? parseFloat(tm_2[key].srate_2) : 0;
                }
                if(tm_2[key].dnbl_2){
                    stats.bowled_2 = 0;
                    stats.overs_2 = 0;
                    stats.maidens_2 = 0;
                    stats.wickets_2 = 0;
                    stats.g_run_2 = 0;
                    stats.economy_2 = 0;
                } else {
                    stats.bowled_2 = 1;
                    stats.overs_2 = tm_2[key].overs_2 ? parseInt(tm_2[key].overs_2) : 0;
                    stats.maidens_2 = tm_2[key].maidens_2 ? parseInt(tm_2[key].maidens_2) : 0;
                    stats.wickets_2 = tm_2[key].wickets_2 ? parseInt(tm_2[key].wickets_2) : 0;
                    stats.g_run_2 = tm_2[key].given_runs_2 ? parseInt(tm_2[key].given_runs_2) : 0;
                    stats.economy_2 = tm_2[key].economy_2 ? parseFloat(tm_2[key].economy_2) : 0;
                }
            }
            for (let p_key in all_players) {
                if (all_players[p_key].key === tm_2[key].key) {
                    let role = all_players[p_key].playingRole.toLowerCase().includes('bowler');
                    let points = calculate(stats, type, role);
                    for(let r = 0; r < all_players[p_key].rounds.length; r++){
                        if(all_players[p_key].rounds[r].key === round_key) {
                            all_players[p_key].rounds[r].played = 1;
                            let tot = 0;
                            if(all_players[p_key].rounds[r].matches) {
                                let found  = false;
                                for(let m in all_players[p_key].rounds[r].matches) {
                                    if(m === match_key) {
                                        found = true;
                                        all_players[p_key].rounds[r].matches[m].stats = stats;
                                        all_players[p_key].rounds[r].matches[m].bonus = points.bonus;
                                        all_players[p_key].rounds[r].matches[m].points = points.total;
                                    }
                                    tot = tot + all_players[p_key].rounds[r].matches[m].points
                                }
                                if(!found){
                                    let data = {
                                        stats: stats,
                                        bonus: points.bonus,
                                        points: points.total
                                    };
                                    all_players[p_key].rounds[r].matches[match_key] = data
                                    tot = tot + points.total
                                }
                            } else {
                                let data = {
                                    stats: stats,
                                    bonus: points.bonus,
                                    points: points.total
                                };
                                all_players[p_key].rounds[r]['matches'] = {}
                                all_players[p_key].rounds[r]['matches'][match_key] = data
                                tot = tot + points.total
                            }
                            all_players[p_key].rounds[r].points = tot;
                            break;
                        }
                    }
                    break;
                }
            }
        }
        admin.database().ref('series/' + tour_key).update({
            pool: all_players,
            score_updated_on: new Date().toISOString()
        }).then(()=> {
            return null
        }).catch((err)=> console.log(err))
    })
});

exports.updateTotalPointsUserSeries = functions.database.ref('series/{tid}/data/updated_info/').onUpdate((snapshot) => {
    let value = snapshot.after.val();
    let round_key = value.round_key;
    let tour_key = snapshot.after.ref.parent.ref.parent.ref.key;
    admin.database().ref('series/' + tour_key).once('value', function(snaps){
        let obj = snaps.val().users;
        let pool = snaps.val().pool;
        let t_data = snaps.val().data;
        let t_users = snaps.val().data.users;
        let max = 0;
        let min = 0;
        let count = 0;
        let all_total = 0;
        let min_id = '';
        let max_id = '';
        for (let key in obj) {
            let totalPoints = 0;
            if(obj[key].tm_crt){
                count++;
                let rounds = obj[key].rounds;
                let i, uRound, uPlayers;
                for(i = 0; i < rounds.length; i++) {
                    if(rounds[i].key === round_key) {
                        uRound = rounds[i];
                        break;
                    }
                }
                if(uRound) {
                    let deduct = uRound.deduct_points ? parseInt(uRound.deduct_points) : 0
                    let players = uRound.in_eleven_wk;
                    players = players.concat(uRound.in_eleven_bts)
                    players = players.concat(uRound.in_eleven_al)
                    players = players.concat(uRound.in_eleven_bow)
                    uPlayers = players;
                    for(let i=0; i < uPlayers.length; i++) {
                        let info = getPlayerRoundInfo(uPlayers[i], pool, round_key)
                        if(info.played) {
                            if(uPlayers[i] === uRound.cap) {
                                totalPoints = totalPoints + parseFloat(info.points * 2);
                            } else if(uPlayers[i] === uRound.vc){
                                totalPoints = totalPoints + parseFloat(info.points * 1.5);
                            } else {
                                totalPoints = totalPoints + info.points;
                            }
                        }
                    }
                    totalPoints = totalPoints - deduct;
                }
                all_total = parseFloat(all_total + totalPoints);
                if(totalPoints >= max) {
                    max = totalPoints;
                    max_id = key;

                }
                if(count === 1) {
                    min = totalPoints
                    min_id = key;
                } else if(totalPoints < min){
                    min = totalPoints;
                    min_id = key;
                }
            }
            if(t_users[key]) {
                if(t_users[key].rounds) {
                    t_users[key].rounds[round_key] = totalPoints
                } else {
                    t_users[key].rounds = {};
                    t_users[key].rounds[round_key] = totalPoints
                }
            }
        }
        if(count > 0) {
            let avg = parseFloat(parseFloat(all_total/count).toFixed())
            for(let i = 0; i < t_data.rounds.length; i++) {
                if(t_data.rounds[i].key === round_key) {
                    t_data.rounds[i].points_added = true;
                    t_data.rounds[i].min_points = min;
                    t_data.rounds[i].max_points = max;
                    t_data.rounds[i].average = avg;
                    t_data.rounds[i].min_id = min_id;
                    t_data.rounds[i].max_id = max_id;
                }
            }
            let objects = t_users;
            for (let u_key in objects) {
                let total = 0;
                let array = objects[u_key];
                for (let r_key in array.rounds) {
                    total = total + array.rounds[r_key]
                }
                objects[u_key].total_points = total
            }
            let sortable = [];
            for (let key in objects) {
                sortable.push([key, objects[key].total_points]);
            }
            sortable.sort((a, b) => (parseFloat(a[1]) < parseFloat(b[1]) ? 1 : -1));
            let rank = 1;
            let prev = sortable[0][1];
            for (let i = 0; i < sortable.length; i++) {
                for (let key in objects) {
                    if(key === sortable[i][0]) {
                        if(parseFloat(sortable[i][1]) < parseFloat(prev)) {
                            rank++
                        }
                        objects[key].prev_rank = objects[key].rank ? objects[key].rank : 0;
                        objects[key].rank = rank;
                        prev = sortable[i][1];
                        break;
                    }
                }
            }
            return admin.database().ref('series/' + tour_key + '/data').update({
                users: t_users,
                rounds: t_data.rounds,
                user_score_updated_on: new Date().toISOString()
            });
        }
    })
});

exports.updateUsersBankSeries = functions.database.ref('/series/{tid}/users/{uid}/bank').onWrite((snapshot, context) => {
    let value =  snapshot.after.val();
    let tour_key = snapshot.after.ref.parent.ref.parent.ref.parent.ref.key;
    let u_key = snapshot.after.ref.parent.ref.key;
    if(context.authType === 'USER' && context.auth.uid === u_key) {
        return admin.database().ref('/series/' + tour_key + '/data/users/' + u_key).update({
            bank: value
        })
    }
});

// Total Points of a player (tournament)
// exports.updateTotalPointsOfAPlayer = functions.database.ref('/tournaments/{tid}/pool/{id}/rounds').onUpdate((snapshot) => {
//     let rounds = snapshot.after.val();
//     let total = 0, i;
//     for(i = 0; i < rounds.length; i++) {
//         total+=parseInt(rounds[i].points)
//     }
//     let tournament = snapshot.after.ref.parent.ref.parent.ref.parent.key;
//     let pool_key = snapshot.after.ref.parent.key;
//     return admin.database().ref('/tournaments/' + tournament + '/pool/' + pool_key).update({
//         total_points: total
//     })
// });


// Total Points of a player (series)
// exports.updateTotalPointsOfAPlayerSeries = functions.database.ref('/series/{tid}/pool/{id}/rounds').onUpdate((snapshot) => {
//     let rounds = snapshot.after.val();
//     let total = 0, i;
//     for(i = 0; i < rounds.length; i++) {
//         total+=parseInt(rounds[i].points)
//     }
//     let tournament = snapshot.after.ref.parent.ref.parent.ref.parent.key;
//     let pool_key = snapshot.after.ref.parent.key;
//     return admin.database().ref('/series/' + tournament + '/pool/' + pool_key).update({
//         total_points: total
//     })
// });

exports.updateRoundToNextSeries = functions.database.ref('series/{tid}/data/round_updated_info/').onUpdate((snapshot) => {
    let activeRound = snapshot.after.val();
    let tour_key = snapshot.after.ref.parent.ref.parent.ref.key;
    admin.database().ref('series/' + tour_key).once('value', function(snaps){
        let obj = snaps.val().users;
        let t_data = snaps.val().data;
        let newRoundKey = activeRound.key;
        for (let key in obj) {
            if(obj[key].tm_crt){
                let rounds = obj[key].rounds;
                let roundKey = 'r' + t_data.act_round;
                let currentRound = {}, exists = false;
                if(rounds) {
                    let i;
                    for(i=0; i < rounds.length; i++) {
                        if(rounds[i].key.toLowerCase() === newRoundKey.toLowerCase()){
                            exists = true;
                            break;
                        }
                        if(rounds[i].key.toLowerCase() === roundKey.toLowerCase()) {
                            currentRound = {
                                key:  newRoundKey,
                                trns_made: 0,
                                deduct_points: 0,
                                in_eleven_wk: rounds[i].in_eleven_wk,
                                in_eleven_bts: rounds[i].in_eleven_bts,
                                in_eleven_al: rounds[i].in_eleven_al,
                                in_eleven_bow: rounds[i].in_eleven_bow,
                                cap: rounds[i].cap,
                                vc: rounds[i].vc
                            };
                        }
                    }
                    if(!exists) rounds.push(currentRound);
                    obj[key].rounds = rounds;
                }
            }
        }
        t_data.act_round = parseInt(activeRound.key.substring(1));
        t_data.fr_trns = activeRound.fr_trns;
        t_data.roundChangedOn = new Date().toISOString()
        return admin.database().ref('series/' + t_data.id).update({
            users: obj,
            data: t_data,
        });
    })
});
exports.updateRoundToNextTournament = functions.database.ref('tournaments/{tid}/data/round_updated_info/').onUpdate((snapshot) => {
    let activeRound = snapshot.after.val();
    let tour_key = snapshot.after.ref.parent.ref.parent.ref.key;
    admin.database().ref('tournaments/' + tour_key).once('value', function(snaps){
        let obj = snaps.val().users;
        let t_data = snaps.val().data;
        let newRoundKey = activeRound.key;
        for (let key in obj) {
            if(obj[key].tm_crt){
                let rounds = obj[key].rounds;
                let roundKey = 'r' + t_data.act_round;
                let currentRound = {}, exists = false;
                if(rounds) {
                    let i;
                    for(i=0; i < rounds.length; i++) {
                        if(rounds[i].key.toLowerCase() === newRoundKey.toLowerCase()){
                            exists = true;
                            break;
                        }
                        if(rounds[i].key.toLowerCase() === roundKey.toLowerCase()) {
                            currentRound = {
                                key:  newRoundKey,
                                trns_made: 0,
                                deduct_points: 0,
                                in_eleven_wk: rounds[i].in_eleven_wk,
                                in_eleven_bts: rounds[i].in_eleven_bts,
                                in_eleven_al: rounds[i].in_eleven_al,
                                in_eleven_bow: rounds[i].in_eleven_bow,
                                in_sub_wk: rounds[i].in_sub_wk,
                                in_sub_other: rounds[i].in_sub_other,
                                cap: rounds[i].cap,
                                vc: rounds[i].vc
                            };
                        }
                    }
                    if(!exists) rounds.push(currentRound);
                    obj[key].rounds = rounds;
                }
            }
        }
        t_data.act_round = parseInt(activeRound.key.substring(1));
        t_data.fr_trns = activeRound.fr_trns;
        t_data.roundChangedOn = new Date().toISOString()
        return admin.database().ref('tournaments/' + t_data.id).update({
            users: obj,
            data: t_data,
        });
    })
});