import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';

import VerificationPage from "../views/verification-page";

const LandingPage = () => import(/* webpackChunkName: "group-landing" */ '../views/landing-page.vue')
const LoginComponent = () => import(/* webpackChunkName: "group-landing" */ '../views/Login.vue')
const RegistrationComponent = () => import(/* webpackChunkName: "group-landing" */ '../views/Registration.vue')
const PointSystemComponent = () => import(/* webpackChunkName: "group-landing" */ '../views/points-calculation.vue')
const PrivacyComponent = () => import(/* webpackChunkName: "group-landing" */ '../views/privacy-policy.vue')
const FaqComponent = () => import(/* webpackChunkName: "group-landing" */ '../views/faq.vue')


const UserHomeComponent = () => import(/* webpackChunkName: "group-user" */ '../views/users/home.vue')
const UserDashboardComponent = () => import(/* webpackChunkName: "group-user" */ '../views/users/dashboard.vue')
const UserProfileComponent = () => import(/* webpackChunkName: "group-user" */ '../views/users/profile.vue')
const UserPointsComponent = () => import(/* webpackChunkName: "group-user" */ '../views/users/points.vue')
const UserTournamentComponent = () => import(/* webpackChunkName: "group-user" */ '../views/users/tournament.vue')
const UserTournamentSingleComponent = () => import(/* webpackChunkName: "group-user" */ '../views/users/tournament-single.vue')
const UserSeriesSingleComponent = () => import(/* webpackChunkName: "group-user" */ '../views/users/Series-Special/series-single.vue')
const UserLeaderBoardComponent = () => import(/* webpackChunkName: "group-user" */ '../views/users/leaderboard.vue')
const UserPointsSystemComponent = () => import(/* webpackChunkName: "group-user" */ '../views/users/points-system.vue')
const UserFixturesComponent = () => import(/* webpackChunkName: "group-user" */ '../views/users/fixtures.vue')
const UserTeamRulesComponent = () => import(/* webpackChunkName: "group-user" */ '../views/users/team-rules.vue')
const UserFaqComponent = () => import(/* webpackChunkName: "group-user" */ '../views/users/faq.vue')
const UserHowTOPlayComponent = () => import(/* webpackChunkName: "group-user" */ '../views/users/how-to-play.vue')
const UserTournamentRulesComponent = () => import(/* webpackChunkName: "group-user" */ '../views/users/rules.vue')
const UserTournamentPrizesComponent = () => import(/* webpackChunkName: "group-user" */ '../views/users/prizes.vue')
const UserSingleMatchComponent = () => import(/* webpackChunkName: "group-user" */ '../views/users/team-single.vue')
const UsersHallofFame = () => import(/* webpackChunkName: "group-user" */ '../views/users/hall-of-fame')
const ClassicComponent = () => import(/* webpackChunkName: "group-user" */ '../views/users/series-mode')
const SingleMatchComponent = () => import(/* webpackChunkName: "group-user" */ '../views/users/match-mode')
const SeriesComponent = () => import(/* webpackChunkName: "group-user" */ '../views/users/series-special-mode')

/// Admin
const AdminHomeComponent = () => import(/* webpackChunkName: "group-admin" */ '../views/admin/home.vue')
const AdminDashboardComponent = () => import(/* webpackChunkName: "group-admin" */ '../views/admin/dashboard.vue')
const AdminProfileComponent = () => import(/* webpackChunkName: "group-admin" */ '../views/admin/profile.vue')
const AdminManagePlayersComponent = () => import(/* webpackChunkName: "group-admin" */ '../views/admin/manage-players.vue')
const AdminManageTournamentsComponent = () => import(/* webpackChunkName: "group-admin" */ '../views/admin/manage-tournaments.vue')
const AdminCreateTournamentsComponent = () => import(/* webpackChunkName: "group-admin" */ '../views/admin/create-tournament-component.vue')
const AdminCreateSeriesSpecialComponent = () => import(/* webpackChunkName: "group-admin" */ '../views/admin/create-series-special.vue')
const AdminManageModeratorsComponent = () => import(/* webpackChunkName: "group-admin" */ '../views/admin/manage-moderators.vue')
const AdminCreateSingleMatch = () => import(/* webpackChunkName: "group-admin" */ '../views/admin/create-single-match.vue')
const AdminManageSingleMatch = () => import(/* webpackChunkName: "group-admin" */ '../views/admin/manage-single-match.vue')
const AdminPushNotification = () => import(/* webpackChunkName: "group-admin" */ '../views/admin/push-notifications.vue')
const AdminManagePrizes = () => import(/* webpackChunkName: "group-admin" */ '../views/admin/manage-prizes.vue')
const AdminManageUsers = () => import(/* webpackChunkName: "group-admin" */ '../views/admin/manage-users.vue')
const AdminManageUsersTournament = () => import(/* webpackChunkName: "group-admin" */ '../views/admin/manage-users-tournament.vue')
const AdminManageHeadToHead = () => import(/* webpackChunkName: "group-admin" */ '../views/admin/manage-head-to-head')
const AdminManageSeriesSpecial = () => import(/* webpackChunkName: "group-admin" */ '../views/admin/manage-series-special')
const AdminAllMatchesWithID = () => import(/* webpackChunkName: "group-admin" */ '../views/admin/all-matches-with-id')
const AdminCreateSeriesCup = () => import(/* webpackChunkName: "group-admin" */ '../views/admin/series-cup/create')
const AdminManageSeriesCup = () => import(/* webpackChunkName: "group-admin" */ '../views/admin/series-cup/manage')


// editor

const EditorHomeComponent = () => import(/* webpackChunkName: "group-editor" */ '../views/editor/home.vue')
const EditorDashboardComponent = () => import(/* webpackChunkName: "group-editor" */ '../views/editor/dashboard.vue')

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: LandingPage,
    meta: {
      isPublic: true
    },
    children: [
      {
        path: '/',
        name: 'home',
        component: Home,
      },
      {
        path: '/login',
        name: 'login',
        component: LoginComponent,
      },
      {
        path: '/registration',
        name: 'registration',
        component: RegistrationComponent,
      },
      {
        path: '/points-system',
        name: 'points-system',
        component: PointSystemComponent,
      },
      {
        path: '/privacy-policy',
        name: 'privacy-policy',
        component: PrivacyComponent,
      },
      {
        path: '/frequently-asked-questions',
        name: 'faq',
        component: FaqComponent,
      },
    ]
  },
  {
    path: "/user",
    name: "user",
    redirect: '/user/dashboard',
    meta: {
      isUser: true
    },
    component: UserHomeComponent,
    children: [
      {
        path: "dashboard",
        name: "dashboard",
        redirect: '/user/dashboard/classic',
        component: UserDashboardComponent,
        children: [
          {
            path: "classic",
            name: "classic",
            component: ClassicComponent
          },
          {
            path: "single-match",
            name: "single-match",
            component: SingleMatchComponent
          },
          {
            path: "series",
            name: "series",
            component: SeriesComponent
          },
        ]
      },
      {
        path: "profile",
        name: "profile",
        component: UserProfileComponent
      },
      {
        path: "my-points",
        name: "my-points",
        component: UserPointsComponent
      },
      {
        path: "my-tournament",
        name: "my-tournament",
        component: UserTournamentComponent,
      },
      {
        path: "tournament",
        name: "tournament",
        component: UserTournamentSingleComponent
      },
      {
        path: "series-home",
        name: "series-home",
        component: UserSeriesSingleComponent
      },
      {
        path: "leaderboard",
        name: "leaderboard",
        component: UserLeaderBoardComponent
      },
      {
        path: "points-distribution",
        name: "points-distribution",
        component: UserPointsSystemComponent
      },
      {
        path: "fixture",
        name: "fixture",
        component: UserFixturesComponent,
      },
      {
        path: "team-rules",
        name: "team-rules",
        component: UserTeamRulesComponent,
      },
      {
        path: "faq",
        name: "faq-home",
        component: UserFaqComponent,
      },
      {
        path: "how-to-play",
        name: "how-to-play",
        component: UserHowTOPlayComponent,
      },
      {
        path: "tournament-rules",
        name: "tournament-rules",
        component: UserTournamentRulesComponent,
      },
      {
          path: "tournament-prizes",
          name: "tournament-prizes",
          component: UserTournamentPrizesComponent,
      },
      {
        path: "match",
        name: "match",
        component: UserSingleMatchComponent
      },
      {
        path: "series-cup",
        name: "series-cup",
        component: UsersHallofFame
      },
    ]
  },
  {
    path: "/admin",
    name: "admin",
    redirect: '/admin/dashboard',
    meta: {
      isAdmin: true
    },
    component: AdminHomeComponent,
    children: [
      {
        path: "dashboard",
        name: "dashboard",
        component: AdminDashboardComponent
      },
      {
        path: "profile",
        name: "profile",
        component: AdminProfileComponent
      },
      {
        path: "manage-players",
        name: "manage-players",
        component: AdminManagePlayersComponent
      },
      {
        path: "manage-tournaments",
        name: "manage-tournaments",
        component: AdminManageTournamentsComponent
      },
      {
        path: "create-tournament",
        name: "create-tournament",
        component: AdminCreateTournamentsComponent
      },
      {
        path: "manage-moderators",
        name: "manage-moderators",
        component: AdminManageModeratorsComponent
      },
      {
        path: "create-single-match",
        name: "create-single-match",
        component: AdminCreateSingleMatch
      },
      {
        path: "create-series-special",
        name: "create-series-special",
        component: AdminCreateSeriesSpecialComponent
      },
      {
        path: "manage-single-match",
        name: "manage-single-match",
        component: AdminManageSingleMatch
      },
      {
          path: "manage-series-special",
          name: "manage-series-special",
          component: AdminManageSeriesSpecial
      },
      {
        path: "push-notification",
        name: "push-notification",
        component: AdminPushNotification
      },
      {
        path: "manage-prizes",
        name: "manage-prizes",
        component: AdminManagePrizes
      },
      {
        path: "manage-users-tournament",
        name: "manage-users-tournament",
        component: AdminManageUsersTournament
      },
      {
        path: "manage-users",
        name: "manage-users",
        component: AdminManageUsers
      },
      {
        path: "all-matches-with-id",
        name: "all-matches-with-id",
        component: AdminAllMatchesWithID
      },
      {
        path: "manage-head-to-head",
        name: "manage-head-to-head",
        component: AdminManageHeadToHead
      },
      {
        path: "create-series-cup",
        name: "create-series-cup",
        component: AdminCreateSeriesCup
      },
      {
        path: "manage-series-cup",
        name: "manage-series-cup",
        component: AdminManageSeriesCup
      },
    ]
  },
  {
    path: "/editor",
    name: "editor",
    redirect: '/editor/dashboard',
    meta: {
      isEditor: true
    },
    component: EditorHomeComponent,
    children: [
      {
        path: "dashboard",
        name: "dashboard",
        component: EditorDashboardComponent
      },
    ]
  },
  {
    path: '/auth',
    component: VerificationPage,
    meta: {
      noAuth: true
    }
  }
]


const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser;
  const requiresAuth = to.matched.some(record => record.meta.isUser)
  const publicPage = to.matched.some(record => record.meta.isPublic)
  const adminPage = to.matched.some(record => record.meta.isAdmin)
  const editorPage = to.matched.some(record => record.meta.isEditor)
  const noAuth = to.matched.some(record => record.meta.noAuth)
  let isAdmin = false;
  let isEditor = false;
  if(noAuth){
    next();
  } else {
    if(currentUser) {
      currentUser.getIdTokenResult().then(function (idTokenResult) {
        isAdmin = !!idTokenResult.claims.moderator;
        if(!isAdmin) {
          isEditor = !!idTokenResult.claims.editor;
        }
        if(publicPage) {
          if(isAdmin) {
            next('/admin');
          }
          else if(isEditor) {
            next('/editor');
          } else {
            next('/user');
          }
        } else if(adminPage) {
          if(isAdmin) {
            next();
          } else {
            if(isEditor) next('/editor')
            else next('/user');
          }
        } else if(editorPage) {
          if(isEditor || isAdmin) {
            next();
          } else {
            next('/user');
          }
        } else if(requiresAuth) {
          if(!isAdmin && !isEditor) {
            next();
          } else {
            if(isAdmin) next('/admin')
            else if(isEditor) next('/editor')
            else firebase.auth().signOut().catch();
          }
        } else {
          next('/login');
        }
      }).catch(function () {
        firebase.auth().signOut().catch();
      });
    } else {
      if(publicPage) {
        next();
      } else {
        next('/login');
      }
    }
  }
});

export default router
