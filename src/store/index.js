import Vue from "vue";
import Vuex from "vuex";
import firebase from 'firebase/app';
import 'firebase/database';

Vue.use(Vuex);

const Deadline = {
  state: {
    time_now: ""
  },
  mutations: {
    auth_success(state, time) {
      let dbTime = new Date(new Date(time).toISOString()).getTime();
      let now = new Date(new Date().toISOString()).getTime();
      let distance = now - dbTime;
      if(distance > 0) {
        state.time_now = new Date(now).toISOString();
      }else {
        state.time_now = new Date(time).toISOString();
      }
    },
  },
  actions: {
    deadlineCallBack({ commit }) {
      return new Promise((resolve) => {
        let database = firebase.database();
        database.ref('time_now').on('value', function(snapshot) {
          commit("auth_success", snapshot.val().timestamp);
          resolve()
        });
      });
    }
  },
  getters: {
    getTimeNow(state) {
      return state.time_now;
    },
  }
};

const store = new Vuex.Store({
  modules: {
    deadline: Deadline,
  }
});

export default store;

