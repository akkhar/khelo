import Vue from "vue";
import store from "../src/store/index.js";
Vue.use(store);
export const getOnlyFirstName = function(name){
    let firstName = name.split(" ");
    let res = firstName[0];
    if(firstName.length > 0) {
        let first = firstName[0];
        let last = firstName[firstName.length - 1];
        res = (last.length > 7 && last.length > first.length) ? first : last;
    }
    return res
};
export const getTruncateNameTeam = function(name){
    let firstName = name.split(" ");
    if(firstName.length > 0) {
        return firstName[firstName.length - 1]
    } else {
        return name
    }
};
export const getDeadLineByCurrentRound = function(active, rounds){
    let i;
    let activeKey = 'r' + active, deadlineStr, deadLineObject;
    for(i = 0; i < rounds.length; i++) {
        if(activeKey.toLowerCase() === rounds[i].key.toLowerCase()) {
            deadlineStr = rounds[i].deadline
        }
    }
    let deadline = new Date(deadlineStr).getTime();
    let isoToday = '';
    if(store.getters.getTimeNow) {
        isoToday = store.getters.getTimeNow;
    } else {
        isoToday = new Date().toISOString()
    }
    let today = new Date(isoToday).getTime();
    let distance = deadline - today;
    let days = Math.floor(distance / (1000 * 60 * 60 * 24));
    let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((distance % (1000 * 60)) / 1000);
    let remaining_time = Math.floor((distance) / (1000 * 60));
    deadLineObject = {
        deadline: new Date(deadlineStr).toString(),
        day: days < 0 ? 0 : days,
        hour: hours < 0 ? 0 : hours,
        minute: minutes < 0 ? 0 : minutes,
        seconds: seconds < 0 ? 0 : seconds,
        distance: distance,
        remaining_time: remaining_time
    };
    return deadLineObject
};

export const getDeadlineMatch = function(deadlineStr){
    let deadLineObject;
    let deadline = new Date(deadlineStr).getTime();
    let isoToday = '';
    if(store.getters.getTimeNow) {
        isoToday = store.getters.getTimeNow;
    } else {
        isoToday = new Date().toISOString()
    }
    let today = new Date(isoToday).getTime();
    let distance = deadline - today;
    let days = Math.floor(distance / (1000 * 60 * 60 * 24));
    let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((distance % (1000 * 60)) / 1000);
    let remaining_time = Math.floor((distance) / (1000 * 60));
    deadLineObject = {
        deadline: new Date(deadlineStr).toString(),
        day: days < 0 ? 0 : days,
        hour: hours < 0 ? 0 : hours,
        minute: minutes < 0 ? 0 : minutes,
        seconds: seconds < 0 ? 0 : seconds,
        distance: distance,
        remaining_time: remaining_time
    };
    return deadLineObject
};

export const IsEmptyObject = function isEmpty(obj) {
    for (let key in obj) {
        /*eslint-disable*/
        if (obj.hasOwnProperty(key)) return false;
    }
    return true;
};

export const generateCode = function () {
    return Math.random().toString(36).substring(2, 8);
};

export const EMAIL_VALIDATOR = function(email) {
    // eslint-disable-next-line
    let email_re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return email_re.test(email);
};